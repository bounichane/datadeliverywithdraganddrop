// Generated by Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
#ifndef CLIENTLIST_SWIFT_H
#define CLIENTLIST_SWIFT_H
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgcc-compat"

#if !defined(__has_include)
# define __has_include(x) 0
#endif
#if !defined(__has_attribute)
# define __has_attribute(x) 0
#endif
#if !defined(__has_feature)
# define __has_feature(x) 0
#endif
#if !defined(__has_warning)
# define __has_warning(x) 0
#endif

#if __has_include(<swift/objc-prologue.h>)
# include <swift/objc-prologue.h>
#endif

#pragma clang diagnostic ignored "-Wauto-import"
#include <Foundation/Foundation.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#if !defined(SWIFT_TYPEDEFS)
# define SWIFT_TYPEDEFS 1
# if __has_include(<uchar.h>)
#  include <uchar.h>
# elif !defined(__cplusplus)
typedef uint_least16_t char16_t;
typedef uint_least32_t char32_t;
# endif
typedef float swift_float2  __attribute__((__ext_vector_type__(2)));
typedef float swift_float3  __attribute__((__ext_vector_type__(3)));
typedef float swift_float4  __attribute__((__ext_vector_type__(4)));
typedef double swift_double2  __attribute__((__ext_vector_type__(2)));
typedef double swift_double3  __attribute__((__ext_vector_type__(3)));
typedef double swift_double4  __attribute__((__ext_vector_type__(4)));
typedef int swift_int2  __attribute__((__ext_vector_type__(2)));
typedef int swift_int3  __attribute__((__ext_vector_type__(3)));
typedef int swift_int4  __attribute__((__ext_vector_type__(4)));
typedef unsigned int swift_uint2  __attribute__((__ext_vector_type__(2)));
typedef unsigned int swift_uint3  __attribute__((__ext_vector_type__(3)));
typedef unsigned int swift_uint4  __attribute__((__ext_vector_type__(4)));
#endif

#if !defined(SWIFT_PASTE)
# define SWIFT_PASTE_HELPER(x, y) x##y
# define SWIFT_PASTE(x, y) SWIFT_PASTE_HELPER(x, y)
#endif
#if !defined(SWIFT_METATYPE)
# define SWIFT_METATYPE(X) Class
#endif
#if !defined(SWIFT_CLASS_PROPERTY)
# if __has_feature(objc_class_property)
#  define SWIFT_CLASS_PROPERTY(...) __VA_ARGS__
# else
#  define SWIFT_CLASS_PROPERTY(...)
# endif
#endif

#if __has_attribute(objc_runtime_name)
# define SWIFT_RUNTIME_NAME(X) __attribute__((objc_runtime_name(X)))
#else
# define SWIFT_RUNTIME_NAME(X)
#endif
#if __has_attribute(swift_name)
# define SWIFT_COMPILE_NAME(X) __attribute__((swift_name(X)))
#else
# define SWIFT_COMPILE_NAME(X)
#endif
#if __has_attribute(objc_method_family)
# define SWIFT_METHOD_FAMILY(X) __attribute__((objc_method_family(X)))
#else
# define SWIFT_METHOD_FAMILY(X)
#endif
#if __has_attribute(noescape)
# define SWIFT_NOESCAPE __attribute__((noescape))
#else
# define SWIFT_NOESCAPE
#endif
#if __has_attribute(ns_consumed)
# define SWIFT_RELEASES_ARGUMENT __attribute__((ns_consumed))
#else
# define SWIFT_RELEASES_ARGUMENT
#endif
#if __has_attribute(warn_unused_result)
# define SWIFT_WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#else
# define SWIFT_WARN_UNUSED_RESULT
#endif
#if __has_attribute(noreturn)
# define SWIFT_NORETURN __attribute__((noreturn))
#else
# define SWIFT_NORETURN
#endif
#if !defined(SWIFT_CLASS_EXTRA)
# define SWIFT_CLASS_EXTRA
#endif
#if !defined(SWIFT_PROTOCOL_EXTRA)
# define SWIFT_PROTOCOL_EXTRA
#endif
#if !defined(SWIFT_ENUM_EXTRA)
# define SWIFT_ENUM_EXTRA
#endif
#if !defined(SWIFT_CLASS)
# if __has_attribute(objc_subclassing_restricted)
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# else
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# endif
#endif
#if !defined(SWIFT_RESILIENT_CLASS)
# if __has_attribute(objc_class_stub)
#  define SWIFT_RESILIENT_CLASS(SWIFT_NAME) SWIFT_CLASS(SWIFT_NAME) __attribute__((objc_class_stub))
#  define SWIFT_RESILIENT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_class_stub)) SWIFT_CLASS_NAMED(SWIFT_NAME)
# else
#  define SWIFT_RESILIENT_CLASS(SWIFT_NAME) SWIFT_CLASS(SWIFT_NAME)
#  define SWIFT_RESILIENT_CLASS_NAMED(SWIFT_NAME) SWIFT_CLASS_NAMED(SWIFT_NAME)
# endif
#endif

#if !defined(SWIFT_PROTOCOL)
# define SWIFT_PROTOCOL(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
# define SWIFT_PROTOCOL_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
#endif

#if !defined(SWIFT_EXTENSION)
# define SWIFT_EXTENSION(M) SWIFT_PASTE(M##_Swift_, __LINE__)
#endif

#if !defined(OBJC_DESIGNATED_INITIALIZER)
# if __has_attribute(objc_designated_initializer)
#  define OBJC_DESIGNATED_INITIALIZER __attribute__((objc_designated_initializer))
# else
#  define OBJC_DESIGNATED_INITIALIZER
# endif
#endif
#if !defined(SWIFT_ENUM_ATTR)
# if defined(__has_attribute) && __has_attribute(enum_extensibility)
#  define SWIFT_ENUM_ATTR(_extensibility) __attribute__((enum_extensibility(_extensibility)))
# else
#  define SWIFT_ENUM_ATTR(_extensibility)
# endif
#endif
#if !defined(SWIFT_ENUM)
# define SWIFT_ENUM(_type, _name, _extensibility) enum _name : _type _name; enum SWIFT_ENUM_ATTR(_extensibility) SWIFT_ENUM_EXTRA _name : _type
# if __has_feature(generalized_swift_name)
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME, _extensibility) enum _name : _type _name SWIFT_COMPILE_NAME(SWIFT_NAME); enum SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_ENUM_ATTR(_extensibility) SWIFT_ENUM_EXTRA _name : _type
# else
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME, _extensibility) SWIFT_ENUM(_type, _name, _extensibility)
# endif
#endif
#if !defined(SWIFT_UNAVAILABLE)
# define SWIFT_UNAVAILABLE __attribute__((unavailable))
#endif
#if !defined(SWIFT_UNAVAILABLE_MSG)
# define SWIFT_UNAVAILABLE_MSG(msg) __attribute__((unavailable(msg)))
#endif
#if !defined(SWIFT_AVAILABILITY)
# define SWIFT_AVAILABILITY(plat, ...) __attribute__((availability(plat, __VA_ARGS__)))
#endif
#if !defined(SWIFT_WEAK_IMPORT)
# define SWIFT_WEAK_IMPORT __attribute__((weak_import))
#endif
#if !defined(SWIFT_DEPRECATED)
# define SWIFT_DEPRECATED __attribute__((deprecated))
#endif
#if !defined(SWIFT_DEPRECATED_MSG)
# define SWIFT_DEPRECATED_MSG(...) __attribute__((deprecated(__VA_ARGS__)))
#endif
#if __has_feature(attribute_diagnose_if_objc)
# define SWIFT_DEPRECATED_OBJC(Msg) __attribute__((diagnose_if(1, Msg, "warning")))
#else
# define SWIFT_DEPRECATED_OBJC(Msg) SWIFT_DEPRECATED_MSG(Msg)
#endif
#if !defined(IBSegueAction)
# define IBSegueAction
#endif
#if __has_feature(modules)
#if __has_warning("-Watimport-in-framework-header")
#pragma clang diagnostic ignored "-Watimport-in-framework-header"
#endif
@import CoreGraphics;
@import Foundation;
@import MessageUI;
@import ObjectiveC;
@import UIKit;
#endif

#pragma clang diagnostic ignored "-Wproperty-attribute-mismatch"
#pragma clang diagnostic ignored "-Wduplicate-method-arg"
#if __has_warning("-Wpragma-clang-attribute")
# pragma clang diagnostic ignored "-Wpragma-clang-attribute"
#endif
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wnullability"

#if __has_attribute(external_source_symbol)
# pragma push_macro("any")
# undef any
# pragma clang attribute push(__attribute__((external_source_symbol(language="Swift", defined_in="ClientList",generated_declaration))), apply_to=any(function,enum,objc_interface,objc_category,objc_protocol))
# pragma pop_macro("any")
#endif

@class UIWindow;

SWIFT_CLASS("_TtC10ClientList11AppDelegate")
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, strong) UIWindow * _Nullable window;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end

@class UITableView;
@class NSNumber;
@class NSIndexPath;
@class UITableViewCell;

SWIFT_CLASS("_TtC10ClientList17ClientsDataSource")
@interface ClientsDataSource : NSObject <UITableViewDataSource>
- (NSInteger)tableView:(UITableView * _Nonnull)tableView numberOfRowsInSection:(NSInteger)section SWIFT_WARN_UNUSED_RESULT;
- (UITableViewCell * _Nonnull)tableView:(UITableView * _Nonnull)tableView cellForRowAtIndexPath:(NSIndexPath * _Nonnull)indexPath SWIFT_WARN_UNUSED_RESULT;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end

@class NSString;
@class NSData;
@class NSProgress;

///
SWIFT_CLASS("_TtC10ClientList11ContactCard")
@interface ContactCard : NSObject <NSItemProviderReading, NSItemProviderWriting>
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, copy) NSArray<NSString *> * _Nonnull readableTypeIdentifiersForItemProvider;)
+ (NSArray<NSString *> * _Nonnull)readableTypeIdentifiersForItemProvider SWIFT_WARN_UNUSED_RESULT;
+ (void)setReadableTypeIdentifiersForItemProvider:(NSArray<NSString *> * _Nonnull)value;
+ (ContactCard * _Nullable)objectWithItemProviderData:(NSData * _Nonnull)data typeIdentifier:(NSString * _Nonnull)typeIdentifier error:(NSError * _Nullable * _Nullable)error SWIFT_WARN_UNUSED_RESULT;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, copy) NSArray<NSString *> * _Nonnull writableTypeIdentifiersForItemProvider;)
+ (NSArray<NSString *> * _Nonnull)writableTypeIdentifiersForItemProvider SWIFT_WARN_UNUSED_RESULT;
+ (void)setWritableTypeIdentifiersForItemProvider:(NSArray<NSString *> * _Nonnull)value;
- (NSProgress * _Nullable)loadDataWithTypeIdentifier:(NSString * _Nonnull)typeIdentifier forItemProviderCompletionHandler:(void (^ _Nonnull)(NSData * _Nullable, NSError * _Nullable))completionHandler SWIFT_WARN_UNUSED_RESULT;
- (nonnull instancetype)init SWIFT_UNAVAILABLE;
+ (nonnull instancetype)new SWIFT_UNAVAILABLE_MSG("-init is unavailable");
@end

@class UILabel;
@class UIImageView;
@class UIBarButtonItem;
@class UIImagePickerController;
@class NSBundle;
@class NSCoder;

SWIFT_CLASS("_TtC10ClientList28ContactDetailsViewController")
@interface ContactDetailsViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, weak) IBOutlet UILabel * _Null_unspecified nameLabel;
@property (nonatomic, weak) IBOutlet UILabel * _Null_unspecified phoneLabel;
@property (nonatomic, weak) IBOutlet UIImageView * _Null_unspecified contactPhoto;
- (void)viewDidLoad;
- (void)chooseImageButtonItemTapped:(UIBarButtonItem * _Null_unspecified)sender;
- (void)sendImageButtonItemTapped:(UIBarButtonItem * _Null_unspecified)sender;
- (void)didReceiveMemoryWarning;
- (void)imagePickerController:(UIImagePickerController * _Nonnull)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey, id> * _Nonnull)info;
- (nonnull instancetype)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)coder OBJC_DESIGNATED_INITIALIZER;
@end

@class MFMailComposeViewController;

@interface ContactDetailsViewController (SWIFT_EXTENSION(ClientList)) <MFMailComposeViewControllerDelegate>
- (void)mailComposeController:(MFMailComposeViewController * _Nonnull)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError * _Nullable)error;
@end

@class UIDragInteraction;
@protocol UIDragSession;
@class UIDragItem;
@class UITargetedDragPreview;

@interface ContactDetailsViewController (SWIFT_EXTENSION(ClientList)) <UIDragInteractionDelegate>
- (NSArray<UIDragItem *> * _Nonnull)dragInteraction:(UIDragInteraction * _Nonnull)interaction itemsForBeginningSession:(id <UIDragSession> _Nonnull)session SWIFT_WARN_UNUSED_RESULT;
- (UITargetedDragPreview * _Nullable)dragInteraction:(UIDragInteraction * _Nonnull)interaction previewForLiftingItem:(UIDragItem * _Nonnull)item session:(id <UIDragSession> _Nonnull)session SWIFT_WARN_UNUSED_RESULT;
@end

@class UIDropInteraction;
@protocol UIDropSession;
@class UIDropProposal;

@interface ContactDetailsViewController (SWIFT_EXTENSION(ClientList)) <UIDropInteractionDelegate>
/// Ensure that the drop session contains a drag item with a data representation
/// that the view can consume.
- (BOOL)dropInteraction:(UIDropInteraction * _Nonnull)interaction canHandleSession:(id <UIDropSession> _Nonnull)session SWIFT_WARN_UNUSED_RESULT;
- (void)dropInteraction:(UIDropInteraction * _Nonnull)interaction sessionDidEnter:(id <UIDropSession> _Nonnull)session;
/// Required delegate method: return a drop proposal, indicating how the
/// view is to handle the dropped items.
- (UIDropProposal * _Nonnull)dropInteraction:(UIDropInteraction * _Nonnull)interaction sessionDidUpdate:(id <UIDropSession> _Nonnull)session SWIFT_WARN_UNUSED_RESULT;
/// This delegate method is the only opportunity for accessing and loading
/// the data representations offered in the drag item.
- (void)dropInteraction:(UIDropInteraction * _Nonnull)interaction performDrop:(id <UIDropSession> _Nonnull)session;
- (void)dropInteraction:(UIDropInteraction * _Nonnull)interaction sessionDidExit:(id <UIDropSession> _Nonnull)session;
- (void)dropInteraction:(UIDropInteraction * _Nonnull)interaction sessionDidEnd:(id <UIDropSession> _Nonnull)session;
@end

@class UIStoryboardSegue;

SWIFT_CLASS("_TtC10ClientList27ContactsTableViewController")
@interface ContactsTableViewController : UITableViewController
- (void)viewDidLoad;
- (void)prepareForSegue:(UIStoryboardSegue * _Nonnull)segue sender:(id _Nullable)sender;
- (nonnull instancetype)initWithStyle:(UITableViewStyle)style OBJC_DESIGNATED_INITIALIZER;
- (nonnull instancetype)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)coder OBJC_DESIGNATED_INITIALIZER;
@end

@class UIDragPreviewParameters;

@interface ContactsTableViewController (SWIFT_EXTENSION(ClientList)) <UITableViewDragDelegate>
/// The <code>tableView(_:itemsForBeginning:at:)</code> method is the essential method
/// to implement for allowing dragging from a table.
- (NSArray<UIDragItem *> * _Nonnull)tableView:(UITableView * _Nonnull)tableView itemsForBeginningDragSession:(id <UIDragSession> _Nonnull)session atIndexPath:(NSIndexPath * _Nonnull)indexPath SWIFT_WARN_UNUSED_RESULT;
/// Multiple dragging:
/// Called to request items to add to an existing drag session in response to the add item gesture.
/// You can use the provided point (in the table view’s coordinate space) to do additional hit testing
/// if desired. If not implemented, or if an empty array is returned, no items will be added to the
/// drag and the gesture will be handled normally.
- (NSArray<UIDragItem *> * _Nonnull)tableView:(UITableView * _Nonnull)tableView itemsForAddingToDragSession:(id <UIDragSession> _Nonnull)session atIndexPath:(NSIndexPath * _Nonnull)indexPath point:(CGPoint)point SWIFT_WARN_UNUSED_RESULT;
/// Allows customization of the preview used for the row when it is lifted or if the drag cancels.
/// If not implemented or if nil is returned, the entire cell will be used for the preview.
- (UIDragPreviewParameters * _Nullable)tableView:(UITableView * _Nonnull)tableView dragPreviewParametersForRowAtIndexPath:(NSIndexPath * _Nonnull)indexPath SWIFT_WARN_UNUSED_RESULT;
/// Called after the lift animation has completed to signal the start of a drag session.
/// This call will always be balanced with a corresponding call to -tableView:dragSessionDidEnd:
- (void)tableView:(UITableView * _Nonnull)tableView dragSessionWillBegin:(id <UIDragSession> _Nonnull)session;
/// Called to signal the end of the drag session.
- (void)tableView:(UITableView * _Nonnull)tableView dragSessionDidEnd:(id <UIDragSession> _Nonnull)session;
@end

@protocol UITableViewDropCoordinator;
@class UITableViewDropProposal;

@interface ContactsTableViewController (SWIFT_EXTENSION(ClientList)) <UITableViewDropDelegate>
/// This delegate method is the only opportunity for accessing and loading the data representations
/// offered in the drag item. The drop coordinator supports accessing the dropped items, updating
/// the table view, and specifying optional animations. Local drags with one item go through the
/// existing <code>tableView(_:moveRowAt:to:)</code> method on the data source.
/// In addition to contact cards, we accept generic strings dropped in which will create a new
/// contact with that string being the new name.
- (void)tableView:(UITableView * _Nonnull)tableView performDropWithCoordinator:(id <UITableViewDropCoordinator> _Nonnull)coordinator;
/// Called frequently while the drop session being tracked inside the table view’s coordinate space.
/// A drop proposal from a table view includes two items:
/// a drop operation (typically .move or .copy),
/// an intent, which declares the action the table view will take upon receiving the items.
/// When the drop is at the end of a section, the destination index path passed will be for a row
/// that does not yet exist (equal to the number of rows in that section), where an inserted row would
/// append to the end of the section. The destination index path may be nil in some circumstances
/// (e.g. when dragging over empty space where there are no cells). Note that in some cases your
/// proposal may not be allowed and the system will enforce a different proposal. You may perform your
/// own hit testing by calling session.location(in:).
/// If you don’t want cells to be inserted as you drag, don’t implement this method.
- (UITableViewDropProposal * _Nonnull)tableView:(UITableView * _Nonnull)tableView dropSessionDidUpdate:(id <UIDropSession> _Nonnull)session withDestinationIndexPath:(NSIndexPath * _Nullable)destinationIndexPath SWIFT_WARN_UNUSED_RESULT;
/// If NO is returned no further delegate methods will be called for this drop session.
/// If not implemented, a default value of YES is assumed.
- (BOOL)tableView:(UITableView * _Nonnull)tableView canHandleDropSession:(id <UIDropSession> _Nonnull)session SWIFT_WARN_UNUSED_RESULT;
/// Called when the drop session is no longer being tracked inside the table view’s coordinate space.
- (void)tableView:(UITableView * _Nonnull)tableView dropSessionDidExit:(id <UIDropSession> _Nonnull)session;
/// Called when the drop session completed, regardless of outcome. Useful for performing any cleanup.
- (void)tableView:(UITableView * _Nonnull)tableView dropSessionDidEnd:(id <UIDropSession> _Nonnull)session;
/// Called when the drop session begins tracking in the table view’s coordinate space.
- (void)tableView:(UITableView * _Nonnull)tableView dropSessionDidEnter:(id <UIDropSession> _Nonnull)session;
@end

#if __has_attribute(external_source_symbol)
# pragma clang attribute pop
#endif
#pragma clang diagnostic pop
#endif
