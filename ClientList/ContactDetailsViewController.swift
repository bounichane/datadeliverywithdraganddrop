/*
Contact Details View.
*/

import UIKit
import MessageUI

class ContactDetailsViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
	var contactCard: ContactCard!

	@IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var contactPhoto: UIImageView!
  
    var imagePicker = UIImagePickerController()
    
    let composeViewController = MFMailComposeViewController()

	// MARK: - View Controller Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

		title = contactCard.name

		navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never

		nameLabel.text = contactCard.name
        phoneLabel.text = contactCard.phoneNumber
        contactPhoto.image = contactCard.photo
        
        // For an image view, you must explicitly enable user interaction to allow drag and drop.
        contactPhoto.isUserInteractionEnabled = true
        
        // Enable dragging from the image view (see ViewController+Drag.swift).
        let dragInteraction = UIDragInteraction(delegate: self)
        contactPhoto.addInteraction(dragInteraction)
        
        // Enable dropping onto the image view (see ViewController+Drop.swift).
        let dropInteraction = UIDropInteraction(delegate: self)
        view.addInteraction(dropInteraction)
        
        //
        self.navigationController?.isToolbarHidden = false
        
        let chooseImageButton = UIBarButtonItem(title: "photo", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.chooseImageButtonItemTapped(_:)))
        
        let sendImageButton = UIBarButtonItem(title: "send", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.sendImageButtonItemTapped(_:)))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        var items = [UIBarButtonItem]()
        items.append(spacer)
        items.append(chooseImageButton)
        items.append(spacer)
        items.append(sendImageButton)
        items.append(spacer)

        toolbarItems = items
    }
    
    @objc func chooseImageButtonItemTapped(_ sender:UIBarButtonItem!)
    {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

          let defaultAction = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert: UIAlertAction!) -> Void in
              self.openCamera()
          })

          let deleteAction = UIAlertAction(title: "Choose Photo", style: .destructive, handler: { (alert: UIAlertAction!) -> Void in
              self.openGallary()
          })

          let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            //  Do something here upon cancellation.
          })

          alertController.addAction(defaultAction)
          alertController.addAction(deleteAction)
          alertController.addAction(cancelAction)
          
          if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender as? UIBarButtonItem
          }

          self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func sendImageButtonItemTapped(_ sender:UIBarButtonItem!)
    {
        let mailComposeViewController = configureMailComposer()
            if MFMailComposeViewController.canSendMail(){
                self.present(mailComposeViewController, animated: true, completion: nil)
            }else{
                print("Can't send email")
            }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Open the camera
    func openCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose image from camera roll
    
    func openGallary(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    print("Button capture")
                    imagePicker.delegate = self
                    imagePicker.sourceType = .savedPhotosAlbum
                    imagePicker.allowsEditing = false

                    present(imagePicker, animated: true, completion: nil)
                }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            picker.dismiss(animated: true, completion: nil)
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                contactPhoto.image = image
            }
        }
    
    func configureMailComposer() -> MFMailComposeViewController{
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients(["benaamer.bounichane@netresto.com"])
        mailComposeVC.setSubject("photo de profile")
        mailComposeVC.setMessageBody("ci-joint ma photo de profile", isHTML: false)
        return mailComposeVC
    }
    
}

// MARK: - MFMailComposeViewControllerDelegate
extension ContactDetailsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
 }
