/*
The Application Delegate.
*/

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	/*
		The app delegate must implement the window property from UIApplicationDelegate
		to use the main storyboard file.
	*/
    var window: UIWindow?
}
